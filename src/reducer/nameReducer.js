import { ADD_NAME, REMOVE_NAME } from "../action/types";

const nameReducer = (state = "", action) => {
  switch (action.type) {
    case ADD_NAME:
      return {action.data};
    default:
      return state;
  }
};

export default nameReducer;
