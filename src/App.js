import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  Button,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function App() {
  const [name, setName] = useState("");

  const storeData = async (value) => {
    try {
      await AsyncStorage.setItem("andiganteng_key", value);
    } catch (error) {
      console.log("error when storeData .", error);
    }
  };

  const getData = async () => {
    try {
      const getName = await AsyncStorage.getItem("andiganteng_key");

      if (getName !== null) {
        setName(getName);
      }
    } catch (error) {
      console.log("error when getData", error);
    }
  };

  const removeData = async () => {
    try {
      await AsyncStorage.removeItem("andiganteng_key");
      setName("");
    } catch (error) {
      console.log("error when removeData", error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Text>Welcome</Text>
        <Text>Nama : {name}</Text>
        <TextInput
          onChange={(e) => storeData(e.target.value)}
          style={styles.inputText}
        />
        <Button onPress={() => getData()} title="Simpan" />
        <Button onPress={() => removeData()} title="Hapus" color="red" />
      </SafeAreaView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  inputText: { borderColor: "red", borderWidth: 1 },
});
