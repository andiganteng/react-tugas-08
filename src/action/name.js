import { ADD_NAME, REMOVE_NAME } from "./types";

export const addName = (name) => {
  return { type: ADD_NAME, name: name };
};

export const rmvName = () => {
  return {
    type: REMOVE_NAME,
  };
};
